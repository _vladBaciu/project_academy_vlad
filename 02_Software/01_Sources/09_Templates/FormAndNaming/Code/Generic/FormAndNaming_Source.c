/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       FormAndNaming_Source.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Example for using the .c file template. The brief should be kept as short as possible (maximum of
 *                three lines, with respect to the given format and without passing over the 120 characters per line),
 *                and only general information should be provided in this section.
 *
 *    Example detailed description of the template header. This paragraph has the purpose of providing details that
 *    cannot be integrated in the brief description, since it would take too much space.
 *    This section is not mandatory and shall be used only when a brief description of the file is not enough. In case
 *    this section is not needed then the line containing the end of comment '*'/ shall be the only line between the
 *    last line of the brief and the border, like in the Empty_Source.c file.
 *    The limit of 120 characters per line applies to all comments and it is also included in the formatter.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "FormAndNaming_Header.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (FORMANDNAMING_GLOBAL_MACRO_LENGTH != 0U)

#if (FORMANDNAMING_GLOBAL_MACRO_LENGTH == 6U)

/** \brief  Example of defining a local macro based on ifdefs. */
#define FORMANDNAMING_LOCAL_MACRO  (6U)

#elif (FORMANDNAMING_GLOBAL_MACRO_LENGTH == 4U)

/** \brief  Example of defining a local macro based on ifdefs. */
#define FORMANDNAMING_LOCAL_MACRO  (4U)

#elif (FORMANDNAMING_GLOBAL_MACRO_LENGTH == 2U)

/** \brief  Example of defining a local macro based on ifdefs. */
#define FORMANDNAMING_LOCAL_MACRO  (2U)

#else

/** \brief  Example of defining a local macro based on ifdefs. */
#define FORMANDNAMING_LOCAL_MACRO  (1U)

#endif /* (FORMANDNAMING_GLOBAL_MACRO_LENGTH == 6U) */

#endif /* (FORMANDNAMING_GLOBAL_MACRO_LENGTH != 0U) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of defining a local constant simple data type. */
typedef uint8 FormAndNaming_LocalSimpleType;

/** \brief  Example of defining a local function type. */
typedef void (FormAndNaming_LocalFunctionType)(void);

/** \brief  Example of defining a local structure data type. */
typedef struct
{
   /** \brief  First member of the example structure type. */
   uint8 uc_UnsignedChar;

   /** \brief  Second member of the example structure type. */
   FormAndNaming_LocalSimpleType t_CustomSimple;

   /** \brief  Third member of the example structure type. */
   FormAndNaming_LocalSimpleType * pt_PointerCustomSimple;

   /** \brief  Fourth member of the example structure type. */
   FormAndNaming_LocalFunctionType * pf_PointerFunction;
} FormAndNaming_LocalStructureType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a local unsigned char variable. */
static uint8 FormAndNaming_uc_LocalVariableUnsignedChar = 0U;

/** \brief  Example of declaring a local unsigned char pointer variable. */
static uint8 * FormAndNaming_puc_LocalVariablePointerUnsignedChar = NULL_PTR;

/** \brief  Example of declaring a local unsigned char array. */
static uint8 FormAndNaming_auc_LocalVariableArrayUnsignedChar[FORMANDNAMING_LOCAL_MACRO] =
   { 0U };

/** \brief  Example of declaring a local signed char variable. */
static sint8 FormAndNaming_sc_LocalVariableSignedChar = 0;

/** \brief  Example of declaring a local signed char pointer variable. */
static sint8 * FormAndNaming_psc_LocalVariablePointerSignedChar = NULL_PTR;

/** \brief  Example of declaring a local signed char array. */
static sint8 FormAndNaming_asc_LocalVariableArraySignedChar[FORMANDNAMING_LOCAL_MACRO] =
   { 0 };

/** \brief  Example of declaring a local unsigned short variable. */
static uint16 FormAndNaming_us_LocalVariableUnsignedShort = 0U;

/** \brief  Example of declaring a local unsigned short pointer variable. */
static uint16 * FormAndNaming_pus_LocalVariablePointerUnsignedShort = NULL_PTR;

/** \brief  Example of declaring a local unsigned short array. */
static uint16 FormAndNaming_aus_LocalVariableArrayUnsignedShort[FORMANDNAMING_LOCAL_MACRO] =
   { 0U };

/** \brief  Example of declaring a local signed short variable. */
static sint16 FormAndNaming_ss_LocalVariableSignedShort = 0;

/** \brief  Example of declaring a local signed short pointer variable. */
static sint16 * FormAndNaming_pss_LocalVariablePointerSignedShort = NULL_PTR;

/** \brief  Example of declaring a local signed short array. */
static sint16 FormAndNaming_ass_LocalVariableArraySignedShort[FORMANDNAMING_LOCAL_MACRO] =
   { 0 };

/** \brief  Example of declaring a local unsigned long variable. */
static uint32 FormAndNaming_ul_LocalVariableUnsignedLong = 0UL;

/** \brief  Example of declaring a local unsigned long pointer variable. */
static uint32 * FormAndNaming_pul_LocalVariablePointerUnsignedLong = NULL_PTR;

/** \brief  Example of declaring a local unsigned long array. */
static uint32 FormAndNaming_aul_LocalVariableArrayUnsignedLong[FORMANDNAMING_LOCAL_MACRO] =
   { 0UL };

/** \brief  Example of declaring a local signed long variable. */
static sint32 FormAndNaming_sl_LocalVariableSignedLong = 0L;

/** \brief  Example of declaring a local signed long pointer variable. */
static sint32 * FormAndNaming_psl_LocalVariablePointerSignedLong = NULL_PTR;

/** \brief  Example of declaring a local signed long array. */
static sint32 FormAndNaming_asl_LocalVariableArraySignedLong[FORMANDNAMING_LOCAL_MACRO] =
   { 0L };

/** \brief  Example of declaring a local boolean variable. */
static boolean FormAndNaming_b_LocalVariableBoolean = TRUE;

/** \brief  Example of declaring a local boolean pointer variable. */
static boolean * FormAndNaming_pb_LocalVariablePointerBoolean = NULL_PTR;

/** \brief  Example of declaring a local boolean array. */
static boolean FormAndNaming_ab_LocalVariableArrayBoolean[FORMANDNAMING_LOCAL_MACRO] =
   { TRUE };

/** \brief  Example of declaring a local custom type variable. */
static FormAndNaming_GlobalStructureType FormAndNaming_t_LocalVariableCustomType =
   {
      0U,
      0U,
      NULL_PTR,
      NULL_PTR
   };

/** \brief  Example of declaring a local custom type pointer variable. */
static FormAndNaming_GlobalStructureType * FormAndNaming_pt_LocalVariablePointerCustomType = NULL_PTR;

/** \brief  Example of declaring a local custom type array. */
static FormAndNaming_GlobalStructureType FormAndNaming_at_LocalVariableArrayCustomType[FORMANDNAMING_LOCAL_MACRO] =
   {
      {
         0U,
         0U,
         NULL_PTR,
         NULL_PTR
      },
   };

/** \brief  Example of declaring a local enum type variable. */
static FormAndNaming_GlobalEnumType FormAndNaming_t_LocalVariableEnum = FORMANDNAMING_VALUE_0;

/** \brief  Example of declaring a local enum pointer variable. */
static FormAndNaming_GlobalEnumType * FormAndNaming_pt_LocalVariablePointerEnum = NULL_PTR;

/** \brief  Example of declaring a local enum array. */
static FormAndNaming_GlobalEnumType FormAndNaming_at_LocalVariableArrayEnum[FORMANDNAMING_LOCAL_MACRO] =
   { FORMANDNAMING_VALUE_0 };

/** \brief  Example of declaring a local function pointer. */
static FormAndNaming_GlobalFunctionType * FormAndNaming_pf_LocalVariablePointerFunction = NULL_PTR;

/** \brief  Example of declaring a local array of function pointers. */
static FormAndNaming_GlobalFunctionType * FormAndNaming_apf_LocalVariableArrayPointerFunction[FORMANDNAMING_LOCAL_MACRO] =
   { NULL_PTR };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a global unsigned char variable. */
uint8 FormAndNaming_guc_GlobalVariableUnsignedChar = 0U;

/** \brief  Example of declaring a global unsigned char pointer. */
uint8 * FormAndNaming_gpuc_GlobalVariablePointerUnsignedChar = NULL_PTR;

/** \brief  Example of declaring a global unsigned char array. */
uint8 FormAndNaming_gauc_GlobalVariableArrayUnsignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { 0U };

/** \brief  Example of declaring a global signed char variable. */
sint8 FormAndNaming_gsc_GlobalVariableSignedChar = 0;

/** \brief  Example of declaring a global signed char pointer. */
sint8 * FormAndNaming_gpsc_GlobalVariablePointerSignedChar = NULL_PTR;

/** \brief  Example of declaring a global signed char array. */
sint8 FormAndNaming_gasc_GlobalVariableArraySignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { 0 };

/** \brief  Example of declaring a global unsigned short variable. */
uint16 FormAndNaming_gus_GlobalVariableUnsignedShort = 0U;

/** \brief  Example of declaring a global unsigned short pointer. */
uint16 * FormAndNaming_gpus_GlobalVariablePointerUnsignedShort = NULL_PTR;

/** \brief  Example of declaring a global unsigned short array. */
uint16 FormAndNaming_gaus_GlobalVariableArrayUnsignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { 0U };

/** \brief  Example of declaring a global signed short variable. */
sint16 FormAndNaming_gss_GlobalVariableSignedShort = 0;

/** \brief  Example of declaring a global signed short pointer. */
sint16 * FormAndNaming_gpss_GlobalVariablePointerSignedShort = NULL_PTR;

/** \brief  Example of declaring a global signed short array. */
sint16 FormAndNaming_gass_GlobalVariableArraySignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { 0 };

/** \brief  Example of declaring a global unsigned long variable. */
uint32 FormAndNaming_gul_GlobalVariableUnsignedLong = 0UL;

/** \brief  Example of declaring a global unsigned long pointer. */
uint32 * FormAndNaming_gpul_GlobalVariablePointerUnsignedLong = NULL_PTR;

/** \brief  Example of declaring a global unsigned long array. */
uint32 FormAndNaming_gaul_GlobalVariableArrayUnsignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { 0UL };

/** \brief  Example of declaring a global signed long variable. */
sint32 FormAndNaming_gsl_GlobalVariableSignedLong = 0L;

/** \brief  Example of declaring a global signed long pointer. */
sint32 * FormAndNaming_gpsl_GlobalVariablePointerSignedLong = NULL_PTR;

/** \brief  Example of declaring a global signed long array. */
sint32 FormAndNaming_gasl_GlobalVariableArraySignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { 0L };

/** \brief  Example of declaring a global boolean variable. */
boolean FormAndNaming_gb_GlobalVariableBoolean = TRUE;

/** \brief  Example of declaring a global boolean pointer. */
boolean * FormAndNaming_gpb_GlobalVariablePointerBoolean = NULL_PTR;

/** \brief  Example of declaring a global boolean array. */
boolean FormAndNaming_gab_GlobalVariableArrayBoolean[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   { TRUE };

/** \brief  Example of declaring a global custom type variable. */
FormAndNaming_GlobalStructureType FormAndNaming_gt_GlobalVariableCustomType =
   {
      0U,
      0U,
      NULL_PTR,
      NULL_PTR,
   };

/** \brief  Example of declaring a global custom type pointer. */
FormAndNaming_GlobalStructureType * FormAndNaming_gpt_GlobalVariablePointerCustomType = NULL_PTR;

/** \brief  Example of declaring a global custom type array. */
FormAndNaming_GlobalStructureType FormAndNaming_gat_GlobalVariableArrayCustomType[FORMANDNAMING_GLOBAL_MACRO_LENGTH] =
   {
      {
         0U,
         0U,
         NULL_PTR,
         NULL_PTR,
      }
   };

/** \brief  Example of declaring a global enum type variable. */
FormAndNaming_GlobalEnumType FormAndNaming_gt_GlobalVariableEnum = FORMANDNAMING_VALUE_0;

/** \brief  Example of declaring a global enum pointer variable. */
FormAndNaming_GlobalEnumType * FormAndNaming_gpt_GlobalVariablePointerEnum = NULL_PTR;

/** \brief  Example of declaring a global enum array. */
FormAndNaming_GlobalEnumType FormAndNaming_gat_GlobalVariableArrayEnum[FORMANDNAMING_LOCAL_MACRO] =
   { FORMANDNAMING_VALUE_0 };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a local constant unsigned char. */
static const uint8 FormAndNaming_kuc_LocalConstantUnsignedChar = 0U;

/** \brief  Example of declaring a local constant unsigned char pointer. */
static const uint8 * FormAndNaming_kpuc_LocalConstantPointerUnsignedChar = NULL_PTR;

/** \brief  Example of declaring a local constant unsigned char array. */
static const uint8 FormAndNaming_kauc_LocalConstantArrayUnsignedChar[FORMANDNAMING_LOCAL_MACRO] =
   { 0U };

/** \brief  Example of declaring a local constant signed char. */
static const sint8 FormAndNaming_ksc_LocalConstantSignedChar = 0;

/** \brief  Example of declaring a local constant signed char pointer. */
static const sint8 * FormAndNaming_kpsc_LocalConstantPointerSignedChar = NULL_PTR;

/** \brief  Example of declaring a local constant signed char array. */
static const sint8 FormAndNaming_kasc_LocalConstantArraySignedChar[FORMANDNAMING_LOCAL_MACRO] =
   { 0 };

/** \brief  Example of declaring a local constant unsigned short. */
static const uint16 FormAndNaming_kus_LocalConstantUnsignedShort = 0U;

/** \brief  Example of declaring a local constant unsigned short pointer. */
static const uint16 * FormAndNaming_kpus_LocalConstantPointerUnsignedShort = NULL_PTR;

/** \brief  Example of declaring a local constant unsigned short array. */
static const uint16 FormAndNaming_kaus_LocalConstantArrayUnsignedShort[FORMANDNAMING_LOCAL_MACRO] =
   { 0U };

/** \brief  Example of declaring a local constant signed short. */
static const sint16 FormAndNaming_kss_LocalConstantSignedShort = 0;

/** \brief  Example of declaring a local constant signed short pointer. */
static const sint16 * FormAndNaming_kpss_LocalConstantPointerSignedShort = NULL_PTR;

/** \brief  Example of declaring a local constant signed short array. */
static const sint16 FormAndNaming_kass_LocalConstantArraySignedShort[FORMANDNAMING_LOCAL_MACRO] =
   { 0 };

/** \brief  Example of declaring a local constant unsigned long. */
static const uint32 FormAndNaming_kul_LocalConstantUnsignedLong =
   { 0UL };

/** \brief  Example of declaring a local constant unsigned long pointer. */
static const uint32 * FormAndNaming_kpul_LocalConstantPointerUnsignedLong = NULL_PTR;

/** \brief  Example of declaring a local constant unsigned long array. */
static const uint32 FormAndNaming_kaul_LocalConstantArrayUnsignedLong[FORMANDNAMING_LOCAL_MACRO] =
   { 0UL };

/** \brief  Example of declaring a local constant signed long. */
static const sint32 FormAndNaming_ksl_LocalConstantSignedLong =
   { 0L };

/** \brief  Example of declaring a local constant signed long pointer. */
static const sint32 * FormAndNaming_kpsl_LocalConstantPointerSignedLong = NULL_PTR;

/** \brief  Example of declaring a local constant signed long array. */
static const sint32 FormAndNaming_kasl_LocalConstantArraySignedLong[FORMANDNAMING_LOCAL_MACRO] =
   { 0L };

/** \brief  Example of declaring a local constant boolean variable. */
static const boolean FormAndNaming_kb_LocalConstantBoolean = TRUE;

/** \brief  Example of declaring a local constant boolean pointer variable. */
static const boolean * FormAndNaming_kpb_LocalConstantPointerBoolean = NULL_PTR;

/** \brief  Example of declaring a local constant boolean array. */
static const boolean FormAndNaming_kab_LocalConstantArrayBoolean[FORMANDNAMING_LOCAL_MACRO] =
   { TRUE };

/** \brief  Example of declaring a local constant custom type. */
static const FormAndNaming_GlobalStructureType FormAndNaming_kt_LocalConstantCustomType =
   {
      0U,
      0U,
      NULL_PTR,
      NULL_PTR
   };

/** \brief  Example of declaring a local constant custom type pointer. */
static const FormAndNaming_GlobalStructureType * FormAndNaming_kpt_LocalConstantPointerCustomType = NULL_PTR;

/** \brief  Example of declaring a local constant custom type array. */
static const FormAndNaming_GlobalStructureType FormAndNaming_kat_LocalConstantArrayCustomType[FORMANDNAMING_LOCAL_MACRO] =
   {
      {
         0U,
         0U,
         NULL_PTR,
         NULL_PTR
      },
   };

/** \brief  Example of declaring a local constant enum type. */
static const FormAndNaming_GlobalEnumType FormAndNaming_kt_LocalConstantEnum = FORMANDNAMING_VALUE_0;

/** \brief  Example of declaring a local constant enum pointer. */
static const FormAndNaming_GlobalEnumType * FormAndNaming_kpt_LocalConstantPointerEnum = NULL_PTR;

/** \brief  Example of declaring a local constant enum array. */
static const FormAndNaming_GlobalEnumType FormAndNaming_at_LocalConstantArrayEnum[FORMANDNAMING_LOCAL_MACRO] =
   { FORMANDNAMING_VALUE_0 };

/** \brief  Example of declaring a local constant function pointer. */
static const FormAndNaming_GlobalFunctionType * FormAndNaming_kpf_LocalConstantPointerFunction = NULL_PTR;

/** \brief  Example of declaring a local constant array of function pointers. */
static const FormAndNaming_GlobalFunctionType * FormAndNaming_kapf_LocalConstantArrayPointerFunction[FORMANDNAMING_LOCAL_MACRO] =
   { NULL_PTR };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a global constant unsigned char. */
const uint8 FormAndNaming_gkuc_GlobalConstantUnsignedChar;

/** \brief  Example of declaring a global constant unsigned char pointer. */
const uint8 * FormAndNaming_gkpuc_GlobalConstantPointerUnsignedChar;

/** \brief  Example of declaring a global constant unsigned char array. */
const uint8 FormAndNaming_gkauc_GlobalConstantArrayUnsignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant signed char. */
const sint8 FormAndNaming_gksc_GlobalConstantSignedChar;

/** \brief  Example of declaring a global constant signed char pointer. */
const sint8 * FormAndNaming_gkpsc_GlobalConstantPointerSignedChar;

/** \brief  Example of declaring a global constant signed char array. */
const sint8 FormAndNaming_gkasc_GlobalConstantArraySignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant unsigned short. */
const uint16 FormAndNaming_gkus_GlobalConstantUnsignedShort;

/** \brief  Example of declaring a global constant unsigned short pointer. */
const uint16 * FormAndNaming_gkpus_GlobalConstantPointerUnsignedShort;

/** \brief  Example of declaring a global constant unsigned short array. */
const uint16 FormAndNaming_gkaus_GlobalConstantArrayUnsignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant signed short. */
const sint16 FormAndNaming_gkss_GlobalConstantSignedShort;

/** \brief  Example of declaring a global constant signed short pointer. */
const sint16 * FormAndNaming_gkpss_GlobalConstantPointerSignedShort;

/** \brief  Example of declaring a global constant signed short array. */
const sint16 FormAndNaming_gkass_GlobalConstantArraySignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant unsigned long. */
const uint32 FormAndNaming_gkul_GlobalConstantUnsignedLong;

/** \brief  Example of declaring a global constant unsigned long pointer. */
const uint32 * FormAndNaming_gkpul_GlobalConstantPointerUnsignedLong;

/** \brief  Example of declaring a global constant unsigned long array. */
const uint32 FormAndNaming_gkaul_GlobalConstantArrayUnsignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant signed long. */
const sint32 FormAndNaming_gksl_GlobalConstantSignedLong;

/** \brief  Example of declaring a global constant signed long pointer. */
const sint32 * FormAndNaming_gkpsl_GlobalConstantPointerSignedLong;

/** \brief  Example of declaring a global constant signed long array. */
const sint32 FormAndNaming_gkasl_GlobalConstantArraySignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant boolean variable. */
const boolean FormAndNaming_gkb_GlobalConstantBoolean = TRUE;

/** \brief  Example of declaring a global constant boolean pointer variable. */
const boolean * FormAndNaming_gkpb_GlobalConstantPointerBoolean = NULL_PTR;

/** \brief  Example of declaring a global constant boolean array. */
const boolean FormAndNaming_gkab_GlobalConstantArrayBoolean[FORMANDNAMING_LOCAL_MACRO] =
   { TRUE };

/** \brief  Example of declaring a global constant custom type. */
const FormAndNaming_GlobalStructureType FormAndNaming_gkt_GlobalConstantCustomType;

/** \brief  Example of declaring a global constant custom type pointer. */
const FormAndNaming_GlobalStructureType * FormAndNaming_gkpt_GlobalConstantPointerCustomType;

/** \brief  Example of declaring a global constant custom type array. */
const FormAndNaming_GlobalStructureType FormAndNaming_gkat_GlobalConstantArrayCustomType[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant enum type. */
extern FormAndNaming_GlobalEnumType FormAndNaming_gkt_GlobalConstantEnum;

/** \brief  Example of declaring a global constant enum type pointer. */
extern FormAndNaming_GlobalEnumType * FormAndNaming_gkpt_GlobalConstantPointerEnum;

/** \brief  Example of declaring a global constant enum type array. */
extern FormAndNaming_GlobalEnumType FormAndNaming_gkat_GlobalConstantArrayEnum[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static void FormAndNaming_v_LocalFunctionNoReturn(uint8 uc_Param0, uint8 uc_Param1);
static uint8 FormAndNaming_uc_LocalFunctionUnsignedCharReturn(uint8 uc_Param);
static uint8 * FormAndNaming_puc_LocalFunctionPointerUnsignedCharReturn(void);
static sint8 FormAndNaming_sc_LocalFunctionSignedCharReturn(sint8 sc_Param);
static sint8 * FormAndNaming_psc_LocalFunctionPointerSignedCharReturn(void);
static uint16 FormAndNaming_us_LocalFunctionUnsignedShortReturn(uint16 us_Param);
static uint16 * FormAndNaming_pus_LocalFunctionPointerUnsignedShortReturn(void);
static sint16 FormAndNaming_ss_LocalFunctionSignedShortReturn(sint16 ss_Param);
static sint16 * FormAndNaming_pss_LocalFunctionPointerSignedShortReturn(void);
static uint32 FormAndNaming_ul_LocalFunctionUnsignedLongReturn(uint32 ul_Param);
static uint32 * FormAndNaming_pul_LocalFunctionPointerUnsignedLongReturn(void);
static sint32 FormAndNaming_sl_LocalFunctionSignedLongReturn(sint32 sl_Param);
static sint32 * FormAndNaming_psl_LocalFunctionPointerSignedLongReturn(void);
static boolean FormAndNaming_b_LocalFunctionBooleanReturn(boolean b_Param);
static boolean * FormAndNaming_pb_LocalFunctionPointerBooleanReturn(void);
static FormAndNaming_GlobalStructureType FormAndNaming_t_LocalFunctionCustomTypeReturn(
   FormAndNaming_GlobalStructureType t_Param);
static FormAndNaming_GlobalStructureType * FormAndNaming_pt_LocalFunctionPointerCustomTypeReturn(void);
static FormAndNaming_GlobalEnumType FormAndNaming_t_LocalFunctionEnumReturn(FormAndNaming_GlobalEnumType t_Param);
static FormAndNaming_GlobalEnumType * FormAndNaming_pt_LocalFunctionPointerEnumReturn(void);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Example of local function.
 * \param      uc_Param0 : First parameter description.
 * \param      uc_Param1 : Second parameter description.
 * \return     -
 */
static void FormAndNaming_v_LocalFunctionNoReturn(uint8 uc_Param0, uint8 uc_Param1)
{
}

/**
 * \brief      Example of local function.
 * \param      uc_Param : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
uint8 FormAndNaming_uc_LocalFunctionUnsignedCharReturn(uint8 uc_Param)
{
   return (uint8) (uc_Param + FormAndNaming_uc_LocalVariableUnsignedChar);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static uint8 * FormAndNaming_puc_LocalFunctionPointerUnsignedCharReturn(void)
{
   return FormAndNaming_puc_LocalVariablePointerUnsignedChar;
}

/**
 * \brief      Example of local function.
 * \param      sc_Param : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
static sint8 FormAndNaming_sc_LocalFunctionSignedCharReturn(sint8 sc_Param)
{
   return (sint8) (sc_Param + FormAndNaming_sc_LocalVariableSignedChar);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static sint8 * FormAndNaming_psc_LocalFunctionPointerSignedCharReturn(void)
{
   return FormAndNaming_psc_LocalVariablePointerSignedChar;
}

/**
 * \brief      Example of local function.
 * \param      us_Param : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
static uint16 FormAndNaming_us_LocalFunctionUnsignedShortReturn(uint16 us_Param)
{
   return (uint16) (us_Param + FormAndNaming_us_LocalVariableUnsignedShort);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static uint16 * FormAndNaming_pus_LocalFunctionPointerUnsignedShortReturn(void)
{
   return FormAndNaming_pus_LocalVariablePointerUnsignedShort;
}

/**
 * \brief      Example of local function.
 * \param      ss_Param : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
static sint16 FormAndNaming_ss_LocalFunctionSignedShortReturn(sint16 ss_Param)
{
   return (sint16) (ss_Param + FormAndNaming_ss_LocalVariableSignedShort);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static sint16 * FormAndNaming_pss_LocalFunctionPointerSignedShortReturn(void)
{
   return FormAndNaming_pss_LocalVariablePointerSignedShort;
}

/**
 * \brief      Example of local function.
 * \param      ul_Param : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
static uint32 FormAndNaming_ul_LocalFunctionUnsignedLongReturn(uint32 ul_Param)
{
   return (uint32) (ul_Param + FormAndNaming_ul_LocalVariableUnsignedLong);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static uint32 * FormAndNaming_pul_LocalFunctionPointerUnsignedLongReturn(void)
{
   return FormAndNaming_pul_LocalVariablePointerUnsignedLong;
}

/**
 * \brief      Example of local function.
 * \param      sl_Param : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
static sint32 FormAndNaming_sl_LocalFunctionSignedLongReturn(sint32 sl_Param)
{
   return (sint32) (sl_Param + FormAndNaming_sl_LocalVariableSignedLong);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static sint32 * FormAndNaming_psl_LocalFunctionPointerSignedLongReturn(void)
{
   return FormAndNaming_psl_LocalVariablePointerSignedLong;
}

/**
 * \brief      Example of local function.
 * \param      b_Param : Example parameter description.
 * \return     Static variable that does nothing since it's just an example.
 */
static boolean FormAndNaming_b_LocalFunctionBooleanReturn(boolean b_Param)
{
   return FormAndNaming_b_LocalVariableBoolean;
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static boolean * FormAndNaming_pb_LocalFunctionPointerBooleanReturn(void)
{
   return FormAndNaming_pb_LocalVariablePointerBoolean;
}

/**
 * \brief      Example of local function.
 * \param      t_Param : Example parameter description.
 * \return     Static variable that does nothing since it's just an example.
 */
static FormAndNaming_GlobalStructureType FormAndNaming_t_LocalFunctionCustomTypeReturn(
   FormAndNaming_GlobalStructureType t_Param)
{
   FormAndNaming_t_LocalVariableCustomType = t_Param;
   return (FormAndNaming_t_LocalVariableCustomType);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static FormAndNaming_GlobalStructureType * FormAndNaming_pt_LocalFunctionPointerCustomTypeReturn(void)
{
   return FormAndNaming_pt_LocalVariablePointerCustomType;
}

/**
 * \brief      Example of local function.
 * \param      t_Param : Example parameter description.
 * \return     Static variable that does nothing since it's just an example.
 */
static FormAndNaming_GlobalEnumType FormAndNaming_t_LocalFunctionEnumReturn(FormAndNaming_GlobalEnumType t_Param)
{
   return (FormAndNaming_GlobalEnumType) (t_Param + FormAndNaming_t_LocalVariableEnum);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
static FormAndNaming_GlobalEnumType * FormAndNaming_pt_LocalFunctionPointerEnumReturn(void)
{
   return FormAndNaming_pt_LocalVariablePointerEnum;
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Example of global function.
 * \param      -
 * \return     -
 */
void FormAndNaming_gv_GlobalFunctionNoReturn(void)
{
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     -
 */
uint8 FormAndNaming_guc_GlobalFunctionUnsignedCharReturn(void)
{
   return FormAndNaming_guc_GlobalVariableUnsignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint8 * FormAndNaming_gpuc_GlobalFunctionPointerUnsignedCharReturn(void)
{
   return FormAndNaming_gpuc_GlobalVariablePointerUnsignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint8 FormAndNaming_gsc_GlobalFunctionSignedCharReturn(void)
{
   return FormAndNaming_gsc_GlobalVariableSignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint8 * FormAndNaming_gpsc_GlobalFunctionPointerSignedCharReturn(void)
{
   return FormAndNaming_gpsc_GlobalVariablePointerSignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint16 FormAndNaming_gus_GlobalFunctionUnsignedShortReturn(void)
{
   return FormAndNaming_gus_GlobalVariableUnsignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint16 * FormAndNaming_gpus_GlobalFunctionPointerUnsignedShortReturn(void)
{
   return FormAndNaming_gpus_GlobalVariablePointerUnsignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint16 FormAndNaming_gss_GlobalFunctionSignedShortReturn(void)
{
   return FormAndNaming_gss_GlobalVariableSignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint16 * FormAndNaming_gpss_GlobalFunctionPointerSignedShortReturn(void)
{
   return FormAndNaming_gpss_GlobalVariablePointerSignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint32 FormAndNaming_gul_GlobalFunctionUnsignedLongReturn(void)
{
   return FormAndNaming_gul_GlobalVariableUnsignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint32 * FormAndNaming_gpul_GlobalFunctionPointerUnsignedLongReturn(void)
{
   return FormAndNaming_gpul_GlobalVariablePointerUnsignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint32 FormAndNaming_gsl_GlobalFunctionSignedLongReturn(void)
{
   return FormAndNaming_gsl_GlobalVariableSignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint32 * FormAndNaming_gpsl_GlobalFunctionPointerSignedLongReturn(void)
{
   return FormAndNaming_gpsl_GlobalVariablePointerSignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
extern boolean FormAndNaming_gb_GlobalFunctionBooleanReturn(void)
{
   return FormAndNaming_gb_GlobalVariableBoolean;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
extern boolean * FormAndNaming_gpb_GlobalFunctionPointerBooleanReturn(void)
{
   return FormAndNaming_gpb_GlobalVariablePointerBoolean;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
FormAndNaming_GlobalStructureType FormAndNaming_gt_GlobalFunctionCustomTypeReturn(void)
{
   return FormAndNaming_gt_GlobalVariableCustomType;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
FormAndNaming_GlobalStructureType * FormAndNaming_gpt_GlobalFunctionPointerCustomTypeReturn(void)
{
   return FormAndNaming_gpt_GlobalVariablePointerCustomType;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
FormAndNaming_GlobalEnumType FormAndNaming_gt_GlobalFunctionEnumReturn(void)
{
   return FormAndNaming_gt_GlobalVariableEnum;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
FormAndNaming_GlobalEnumType * FormAndNaming_gpt_GlobalFunctionPointerEnumReturn(void)
{
   return FormAndNaming_gpt_GlobalVariablePointerEnum;
}
