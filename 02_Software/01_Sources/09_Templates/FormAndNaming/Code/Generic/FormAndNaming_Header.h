/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       FormAndNaming_Header.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Example for using the .h file template. The brief should be kept as short as possible (maximum of
 *                three lines, with respect to the given format and without passing over the 120 characters per line),
 *                and only general information should be provided in this section.
 *
 *    Example detailed description of the template header. This paragraph has the purpose of providing details that
 *    cannot be integrated in the brief description, since it would take too much space.
 *    This section is not mandatory and shall be used only when a brief description of the file is not enough. In case
 *    this section is not needed then the line containing the end of comment '*'/ shall be the only line between the
 *    last line of the brief and the border, like in the Empty_Header.h file.
 *    The limit of 120 characters per line applies to all comments and it is also included in the formatter.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef FORMANDNAMING_HEADER_H
#define FORMANDNAMING_HEADER_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of an macro defining the lengths of the exported arrays. */
#define FORMANDNAMING_GLOBAL_MACRO_LENGTH (1U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of an exported simple data type. */
typedef uint8 FormAndNaming_GlobalSimpleType;

/** \brief  Example of an exported function type. */
typedef void (FormAndNaming_GlobalFunctionType)(void);

/** \brief  Example of an exported structure data type. */
typedef struct
{
   /** \brief  First member of the example structure type. */
   uint8 uc_UnsignedChar;

   /** \brief  Second member of the example structure type. */
   FormAndNaming_GlobalSimpleType t_CustomSimple;

   /** \brief  Third member of the example structure type. */
   FormAndNaming_GlobalSimpleType * pt_PointerCustomSimple;

   /** \brief  Fourth member of the example structure type. */
   FormAndNaming_GlobalFunctionType * pf_PointerFunction;
} FormAndNaming_GlobalStructureType;

/** \brief  Defines all the possible values of an enumeration type. */
typedef enum
{
   /** \brief  The first value of the enumeration. */
   FORMANDNAMING_VALUE_0,
   /** \brief  The second value of the enumeration. */
   FORMANDNAMING_VALUE_1,
   /** \brief  The third value of the enumeration. */
   FORMANDNAMING_VALUE_2,
   /** \brief  The fourth value of the enumeration. */
   FORMANDNAMING_VALUE_3,
} FormAndNaming_GlobalEnumType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern uint8 FormAndNaming_guc_GlobalVariableUnsignedChar;
extern uint8 * FormAndNaming_gpuc_GlobalVariablePointerUnsignedChar;
extern uint8 FormAndNaming_gauc_GlobalVariableArrayUnsignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern sint8 FormAndNaming_gsc_GlobalVariableSignedChar;
extern sint8 * FormAndNaming_gpsc_GlobalVariablePointerSignedChar;
extern sint8 FormAndNaming_gasc_GlobalVariableArraySignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern uint16 FormAndNaming_gus_GlobalVariableUnsignedShort;
extern uint16 * FormAndNaming_gpus_GlobalVariablePointerUnsignedShort;
extern uint16 FormAndNaming_gaus_GlobalVariableArrayUnsignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern sint16 FormAndNaming_gss_GlobalVariableSignedShort;
extern sint16 * FormAndNaming_gpss_GlobalVariablePointerSignedShort;
extern sint16 FormAndNaming_gass_GlobalVariableArraySignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern uint32 FormAndNaming_gul_GlobalVariableUnsignedLong;
extern uint32 * FormAndNaming_gpul_GlobalVariablePointerUnsignedLong;
extern uint32 FormAndNaming_gaul_GlobalVariableArrayUnsignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern sint32 FormAndNaming_gsl_GlobalVariableSignedLong;
extern sint32 * FormAndNaming_gpsl_GlobalVariablePointerSignedLong;
extern sint32 FormAndNaming_gasl_GlobalVariableArraySignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern boolean FormAndNaming_gb_GlobalVariableBoolean;
extern boolean * FormAndNaming_gpb_GlobalVariablePointerBoolean;
extern boolean FormAndNaming_gab_GlobalVariableArrayBoolean[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern FormAndNaming_GlobalStructureType FormAndNaming_gt_GlobalVariableCustomType;
extern FormAndNaming_GlobalStructureType * FormAndNaming_gpt_GlobalVariablePointerCustomType;
extern FormAndNaming_GlobalStructureType FormAndNaming_gat_GlobalVariableArrayCustomType[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern FormAndNaming_GlobalEnumType FormAndNaming_gt_GlobalVariableEnum;
extern FormAndNaming_GlobalEnumType * FormAndNaming_gpt_GlobalVariablePointerEnum;
extern FormAndNaming_GlobalEnumType FormAndNaming_gat_GlobalVariableArrayEnum[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const uint8 FormAndNaming_gkuc_GlobalConstantUnsignedChar;
extern const uint8 * FormAndNaming_gkpuc_GlobalConstantPointerUnsignedChar;
extern const uint8 FormAndNaming_gkauc_GlobalConstantArrayUnsignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const sint8 FormAndNaming_gksc_GlobalConstantSignedChar;
extern const sint8 * FormAndNaming_gkpsc_GlobalConstantPointerSignedChar;
extern const sint8 FormAndNaming_gkasc_GlobalConstantArraySignedChar[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const uint16 FormAndNaming_gkus_GlobalConstantUnsignedShort;
extern const uint16 * FormAndNaming_gkpus_GlobalConstantPointerUnsignedShort;
extern const uint16 FormAndNaming_gkaus_GlobalConstantArrayUnsignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const sint16 FormAndNaming_gkss_GlobalConstantSignedShort;
extern const sint16 * FormAndNaming_gkpss_GlobalConstantPointerSignedShort;
extern const sint16 FormAndNaming_gkass_GlobalConstantArraySignedShort[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const uint32 FormAndNaming_gkul_GlobalConstantUnsignedLong;
extern const uint32 * FormAndNaming_gkpul_GlobalConstantPointerUnsignedLong;
extern const uint32 FormAndNaming_gkaul_GlobalConstantArrayUnsignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const sint32 FormAndNaming_gksl_GlobalConstantSignedLong;
extern const sint32 * FormAndNaming_gkpsl_GlobalConstantPointerSignedLong;
extern const sint32 FormAndNaming_gkasl_GlobalConstantArraySignedLong[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const boolean FormAndNaming_gkb_GlobalConstantBoolean;
extern const boolean * FormAndNaming_gkpb_GlobalConstantPointerBoolean;
extern const boolean FormAndNaming_gkab_GlobalConstantArrayBoolean[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern const FormAndNaming_GlobalStructureType FormAndNaming_gkt_GlobalConstantCustomType;
extern const FormAndNaming_GlobalStructureType * FormAndNaming_gkpt_GlobalConstantPointerCustomType;
extern const FormAndNaming_GlobalStructureType FormAndNaming_gkat_GlobalConstantArrayCustomType[FORMANDNAMING_GLOBAL_MACRO_LENGTH];
extern FormAndNaming_GlobalEnumType FormAndNaming_gkt_GlobalConstantEnum;
extern FormAndNaming_GlobalEnumType * FormAndNaming_gkpt_GlobalConstantPointerEnum;
extern FormAndNaming_GlobalEnumType FormAndNaming_gkat_GlobalConstantArrayEnum[FORMANDNAMING_GLOBAL_MACRO_LENGTH];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void FormAndNaming_gv_GlobalFunctionNoReturn(void);
extern uint8 FormAndNaming_guc_GlobalFunctionUnsignedCharReturn(void);
extern uint8 * FormAndNaming_gpuc_GlobalFunctionPointerUnsignedCharReturn(void);
extern sint8 FormAndNaming_gsc_GlobalFunctionSignedCharReturn(void);
extern sint8 * FormAndNaming_gpsc_GlobalFunctionPointerSignedCharReturn(void);
extern uint16 FormAndNaming_gus_GlobalFunctionUnsignedShortReturn(void);
extern uint16 * FormAndNaming_gpus_GlobalFunctionPointerUnsignedShortReturn(void);
extern sint16 FormAndNaming_gss_GlobalFunctionSignedShortReturn(void);
extern sint16 * FormAndNaming_gpss_GlobalFunctionPointerSignedShortReturn(void);
extern uint32 FormAndNaming_gul_GlobalFunctionUnsignedLongReturn(void);
extern uint32 * FormAndNaming_gpul_GlobalFunctionPointerUnsignedLongReturn(void);
extern sint32 FormAndNaming_gsl_GlobalFunctionSignedLongReturn(void);
extern sint32 * FormAndNaming_gpsl_GlobalFunctionPointerSignedLongReturn(void);
extern boolean FormAndNaming_gb_GlobalFunctionBooleanReturn(void);
extern boolean * FormAndNaming_gpb_GlobalFunctionPointerBooleanReturn(void);
extern FormAndNaming_GlobalStructureType FormAndNaming_gt_GlobalFunctionCustomTypeReturn(void);
extern FormAndNaming_GlobalStructureType * FormAndNaming_gpt_GlobalFunctionPointerCustomTypeReturn(void);
extern FormAndNaming_GlobalEnumType FormAndNaming_gt_GlobalFunctionEnumReturn(void);
extern FormAndNaming_GlobalEnumType * FormAndNaming_gpt_GlobalFunctionPointerEnumReturn(void);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* FORMANDNAMING_HEADER_H */
