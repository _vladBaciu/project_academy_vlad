import os.path
from tkinter import messagebox
import re

H_FILE_N_POS = 2
H_AUTHOR_POS = 3
H_BRIEF_POS = 4
H_IFNDEF_POS = 9
H_DEF_POS = 10
H_INCLUSIONS_POS = 15
H_G_MACRO_POS = 19
H_G_DTYPE_POS = 23
H_G_VAR_POS = 27
H_G_CONST_POS = 31
H_G_FUNC_POS = 35
H_ENDIF_POS = 37

C_FILE_N_POS = 2
C_AUTHOR_POS = 3
C_BRIEF_POS = 4
C_INCLUSIONS_POS = 12
C_L_MACRO_POS = 16
C_L_DTYPE_POS = 20
C_L_VAR_POS = 24
C_G_VAR_POS = 28
C_L_CONST_POS = 32
C_G_CONST_POS = 36
C_L_FUNC_POS = 40
C_L_IMPL_FUNC_POS = 44
C_G_IMPL_FUNC_POS = 48

AUTHOR_N = "RTE Generator"
BRIEF = "Generated file - shall not be manually edited. "
BRIEF_RTE_C = " *                Implements the RTE initialization function which loads the RTE buffers with start values."
BRIEF_TASKS_C = " *                Implements all the OS tasks. Maps software component runnables to tasks."
BRIEF_BUFFERS_C = " *                Declares all the RTE buffers that are used in the read / write operations."
BRIEF_BUFFERS_H = " *                Exports all the RTE buffers that are used in the read / write operations."
BRIEF_MODULE_H = " *                Implements all the RTE read and write operations that are performed by the SwcExample software\n *                component."
BRIEF_TYPES_H = " *                Exports only the software component runnables (global functions), usually only one initialization\n *                and one cyclic runnable. This header must exist for every software component and its name shall be\n *                the short name of the component that is used as a prefix for all the global functions and variables\n *                inside the module."
TYPEDEF_NEWT_POS = 'B'
TYPEDEF_BASET_POS = 'C'

ENUM_NEWT_POS = 'B'
ENUM_MEMBERN_POS = 'C'
ENUM_MEMBER_VAL_POS = 'D'

STRUCT_NEWT_POS = 'B'
STRUCT_MEMBERT_POS = 'C'
STRUCT_MEMBERN_POS = 'D'

BUFF_INTERF_POS = 'B'
BUFF_VAR_PROT_POS = 'C'
BUFF_DTYPE_POS = 'D'
BUFF_INIT_VAL_POS = 'E'
BUFF_UPDATE_FLAG_POS = 'F'

IOHWAB_INTERF_POS = 'B'
IOHWAB_ITYPE_POS = 'C'
IOHWAB_VAR_PROT_POS = 'D'
IOHWAB_CH_N_POS = 'E'

MODULE_PORT_N_POS = 'B'
MODULE_PORT_TYPE_POS = 'C'
MODULE_USEDINTERF_POS = 'D'
MODULE_RUNNABLE_POS = 'E'

MAIN_MODULE_COL_POS = 'B'

CORE_CLUSTER_POS = 'B'
CORE_INTERFTYPE_POS = 'C'
CORE_DIR_POS = 'D'
CORE_FUNC_N_POS = 'E'

CORE_SDTYPE_N_POS = 'B'

SCHEDULING_INIT = 'B'
SCHEDULING_TASK_N = 'C'
SCHEDULING_RUNNABLES = 'D'

START_CELL_POS = 4
MINUS_VAL = 3

TOTAL_LINES_POS = "A1"
TOTAL_LINES_RUN_POS = "E1"
TOTAL_LINES_INIT_POS = "B1"

dirname = os.path.abspath(os.path.dirname(__file__))

class Log:
	def __init__(self):
		self.errors = ""
		self.warnings = ""

def check_name(name):
	match = re.match(r'^[A-Z][a-zA-Z0-9]{1,19}$', name)
	if match != None:
		return 0
	
	return 1

def check_enummember(name):
	match = re.match(r'^RTE_[A-Z][A-Z0-9_]{1,29}$', name)
	if match != None:
		return 0
	return 1

def check_structprimmem(typename, memname):
	if (typename == "sint8"):
		match = re.match(r'^sc_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	elif (typename == "uint8"):
		match = re.match(r'^uc_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	elif (typename == "sint16"):
		match = re.match(r'^ss_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	elif (typename == "uint16"):
		match = re.match(r'^us_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	elif (typename == "sint32"):
		match = re.match(r'^sl_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	elif (typename == "uint32"):
		match = re.match(r'^ul_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	elif (typename == "boolean"):
		match = re.match(r'^b_[A-Z][a-zA-Z0-9]{1,29}$', memname)
	if match != None:
		return 0
	return 1

def check_structcustmem(name):
	match = re.match(r'^t_[A-Z][a-zA-Z0-9]{1,29}$', name)
	if match != None:
		return 0
	return 1

def check_ostask(name):
	match = re.match(r'^OS_[A-Z0-9_]{1,29}_TASK$', name)
	if match != None:
		return 0
	return 1

def check_iohwabchannel(name):
	match = re.match(r'^IOHWAB_[A-Z][A-Z0-9_]{1,29}$', name)
	if match != None:
		return 0
	return 1

def check_typename(name):
	match = re.match(r'^Rte_[A-Z][a-zA-Z0-9]{1,19}Type$', name)
	if match != None:
		return 0
	
	return 1

def check_var(runnable):
	match = re.match(r'^[A-Z][a-zA-Z0-9_]{1,29}$', runnable)
	if match != None:
		return 0
	
	return 1

def check_init(type, value):
	if type == "uint8":
		match = re.match(r'(^0U$)|(^[1-9][0-9]{0,2}U$)', value)
		if match != None:
			int_value = int(value[0:-1])
			if int_value >= 0 and int_value <= 255:
				return 0
		match = re.match(r'(^0[xX][0-9a-fA-F]{1,2}U$)', value)
		if match != None:
			int_value = int(value[0:-1], 16)
			if int_value >= 0 and int_value <= 255:
				return 0
	elif type == "uint16":
		match = re.match(r'(^0U$)|(^[1-9][0-9]{0,4}U$)', value)
		if match != None:
			int_value = int(value[0:-1])
			if int_value >= 0 and int_value <= 65535:
				return 0
		match = re.match(r'(^0[xX][0-9a-fA-F]{1,4}U$)', value)
		if match != None:
			int_value = int(value[0:-1], 16)
			if int_value >= 0 and int_value <= 65535:
				return 0
	elif type == "uint32":
		match = re.match(r'(^0UL$)|(^[1-9][0-9]{0,9}UL$)', value)
		if match != None:
			int_value = int(value[0:-2])
			if int_value >= 0 and int_value <= 4294967295:
				return 0
		match = re.match(r'(^0[xX][0-9a-fA-F]{1,8}UL$)', value)
		if match != None:
			int_value = int(value[0:-2], 16)
			if int_value >= 0 and int_value <= 4294967295:
				return 0
	elif type == "sint8":
		match = re.match(r'(^[-+]{0,1}0$)|(^[-+]{0,1}[1-9][0-9]{0,2}$)', value)
		if match != None:
			int_value = int(value[0:-1])
			if int_value >= -128 and int_value <= 127:
				return 0
	elif type == "sint16":
		match = re.match(r'(^[-+]{0,1}0$)|(^[-+]{0,1}[1-9][0-9]{0,4}$)', value)
		if match != None:
			int_value = int(value[0:-1])
			if int_value >= -32768 and int_value <= 32767:
				return 0
	elif type == "sint32":
		match = re.match(r'(^[-+]{0,1}0$)|(^[-+]{0,1}[1-9][0-9]{0,9}$)', value)
		if match != None:
			int_value = int(value[0:-1])
			if int_value >= -2147483648 and int_value <= 2147483647:
				return 0
	elif type == "boolean":
		match = re.match(r'(^(TRUE)|(FALSE)$)', value)
		if match != None:
			return 0
	
	return 1